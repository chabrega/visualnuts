const minLimit = 1;    // Starts from
const maxLimit = 100;  // Going up to

/**
 * Prints the whole numbers between two integer values.
 */
function printResult() {
    for (let i = minLimit; i <= maxLimit; i++) {
        console.log(getResponse(i));
    }
}

/**
 * Returns number or string depending on:
 * - The parameter is divisible by 3 and / or 5? then returns a string
 * - No? just return the parameter itself
 * 
 * @param  {Number} iteratedNumber The iterated number
 * @return {(Number|String)}
 */
function getResponse(iteratedNumber) {
    if (typeof (iteratedNumber) !== "number") throw new Error ('The parameter should be type Number');

    if (iteratedNumber % 3 === 0 && iteratedNumber % 5 === 0) return 'Visual Nuts';
    if (iteratedNumber % 3 === 0) return 'Nuts';
    if (iteratedNumber % 5 === 0) return 'Visual';



    return iteratedNumber;
}

// Call the main function
printResult();