// Json object
const countries = [{ "country": "US", "languages": ["en"] }, { "country": "BE", "languages": ["nl", "fr", "de"] }, { "country": "NL", "languages": ["nl", "fy"] }, { "country": "DE", "languages": ["de"] }, { "country": "ES", "languages": ["es"] }];

/**
 * Returns the number of countries in the world
 * 
 * @return {Number}
 */
function getNumberOfCountriesInTheWorld() {
    return Object.keys(countries).length;
}

/**
 * Returns the country with most official languages by
 * a given language.
 * 
 * @param  {String} language The given language code
 * @return {String} The country with most official languages
 */
function getCountryWithMostOfficialLanguagesByLanguage(language) {
    let countryWithMostOfficialLanguages = "";
    let languagesQuantityTemp = 0;

    countries.forEach(function (item) {
        if (
            item.languages.includes(language) &&
            item.languages.length > languagesQuantityTemp
        ) {
            countryWithMostOfficialLanguages = item.country;
            languagesQuantityTemp = item.languages.length;
        }
    });

    return countryWithMostOfficialLanguages;
}

/**
 * Returns the official languages for each country in the json.
 * 
 * @return {Array} Object list with country => number_of_languages
 */
function countOfficialLanguagesForEachCountry() {
    let finalResult = [];

    countries.forEach(function (item) {
        finalResult.push({
            'country': item.country,
            'number_of_languages': item.languages.length
        })
    });

    return finalResult;
}

/**
 * Returns the country with highest number of official languages
 * 
 * @return {String} The country name
 */
function getCountryWithHighestNumberOfOfficialLanguages() {
    let highestCountryName = "";
    let highestCountryValueTemp = 0;

    countries.forEach(function (item) {
        if (item.languages.length > highestCountryValueTemp) {
            highestCountryName = item.country;
            highestCountryValueTemp = item.languages.length;
        }
    });

    return highestCountryName;
}

/**
 * Returns the most common official language(s)
 * 
 * @return {Array} Return array with one or more elements
 */
function getMostCommonOfficialLanguage() {
    let allOccurences = [];

    countries.forEach(function (item) {
        item.languages.forEach(function (language) {
            allOccurences.push(language);
        });
    });

    let allCounts = allOccurences.reduce((a, c) => {
        a[c] = (a[c] || 0) + 1;
        return a;
    }, {});
    let maxCount = Math.max(...Object.values(allCounts));
    let mostFrequentOfficialLanguages = Object.keys(allCounts).filter(k => allCounts[k] === maxCount);

    return mostFrequentOfficialLanguages;
}

console.log("Nº of Countries in World:" + getNumberOfCountriesInTheWorld());

console.log("Countries With Most Official Languages DE:" + getCountryWithMostOfficialLanguagesByLanguage('de'));

console.log("Count official Languages for each country" , countOfficialLanguagesForEachCountry());

console.log("Country with highest official languages:" + getCountryWithHighestNumberOfOfficialLanguages());

console.log("Most Common official language:" + getMostCommonOfficialLanguage());